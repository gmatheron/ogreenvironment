FROM ubuntu

RUN apt-get update

WORKDIR /root

RUN apt-get -y install aptitude
RUN aptitude -y install build-essential automake libtool libfreetype6-dev libfreeimage-dev libzzip-dev libxrandr-dev libxaw7-dev freeglut3-dev libgl1-mesa-dev libglu1-mesa-dev wget curl cmake Xorg libzzip-dev libois-dev libboost-thread-dev doxygen graphviz libcppunit-dev git

RUN curl https://pkg-config.freedesktop.org/releases/pkg-config-0.28.tar.gz -o pkgconfig.tgz && \
    tar -zxf pkgconfig.tgz && cd pkg-config-0.28 && \
    ./configure --with-internal-glib && make install && \
    cd .. && \
    rm -rf pkg*

RUN  wget -O freetype.tar.bz2 "http://downloads.sourceforge.net/project/freetype/freetype2/2.6.2/freetype-2.6.2.tar.bz2?r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Ffreetype%2Ffiles%2Ffreetype2%2F&ts=1455253739&use_mirror=netcologne"
RUN tar xvjf freetype.tar.bz2 && \
    cd /root/freetype-2.6.2 && \
    ./configure && \
    make && \
    make install && \
    cd .. && \
    rm -rf freetype*

RUN git clone https://github.com/wgois/OIS.git && \
    cd OIS && \
    ./bootstrap && \
    ./configure && \
    make -j4 && make install && \
    cd .. && \
    rm -rf OIS

RUN wget -O ogre.tar.bz2 "http://downloads.sourceforge.net/project/ogre/ogre/1.8/1.8.1/ogre_src_v1-8-1.tar.bz2?r=http%3A%2F%2Fwww.ogre3d.org%2Fdownload%2Fsource&ts=1455246920&use_mirror=iweb" && \
    tar xvjf ogre.tar.bz2 && \
    cd ogre_src_v1-8-1 && \
    cmake . && \
    make -j4 && \
    make install && \
    cd && \
    rm -rf ogre*


ENV PKG_CONFIG_PATH /root/ois-v1-3
ENV LD_LIBRARY_PATH /usr/local/lib/:/usr/lib:/usr/local/lib/OGRE/

ENTRYPOINT ["/bin/bash"]

